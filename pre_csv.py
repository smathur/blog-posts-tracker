# DUO PROMPT: Write a script that iterates through every folder in the given path. Within each folder, scan every .yml file and save the value on the lines that start with "name:", "reports_to:", "gitlab:", "job_title:", and "division:". Save these values into a DataFrame and then export the DataFrame as a CSV titled team_members.csv.
import os
import pandas as pd
import yaml

def get_team_members(path):
    df = pd.DataFrame(columns=["name", "reports_to", "gitlab", "job_title", "division"])
    for folder in os.listdir(path):
        folder_path = os.path.join(path, folder)
        if os.path.isdir(folder_path):
            for file in os.listdir(folder_path):
                file_path = os.path.join(folder_path, file)
                if file.endswith('.yml'):
                    with open(file_path, 'r') as f:
                        lines = f.readlines()

                    # Create a dictionary to store the values
                    data = {}
                    for line in lines:
                        if line.startswith("name:"):
                            data["name"] = line.split(":")[1].strip()
                        elif line.startswith("reports_to:"):
                            # DUO PROMPT: Save the value of reports_to as a variable. Then in the parent directory, go to the directory whose name is the first character of the reports_to value. In that directory, find the .yml file starting with the reports_to value, and get the line in that file that starts with "name:"
                            reports_to = line.split(":")[1].strip()
                            reports_to_dir = os.path.join(path, reports_to[0])
                            reports_to_file = [f for f in os.listdir(reports_to_dir) if f.endswith('.yml') and f.startswith(reports_to)][0]
                            reports_to_file_path = os.path.join(reports_to_dir, reports_to_file)
                            with open(reports_to_file_path, 'r') as f:
                                reports_to_lines = f.readlines()
                            for line in reports_to_lines:
                                if line.startswith("name:"):
                                    data["reports_to"] = line.split(":")[1].strip()
                        elif line.startswith("gitlab:"):
                            data["gitlab"] = line.split(":")[1].strip()
                        elif line.startswith("job_title:"):
                            data["job_title"] = line.split(":")[1].strip()
                        elif line.startswith("division:"):
                            data["division"] = line.split(":")[1].strip()
                    
                    # Add the data to the dataframe
                    df = df.append(data, ignore_index=True)
                    print(data)

    return df

if __name__ == '__main__':
    path = os.path.join(os.environ["FOLDER"], os.environ["REPO_DIR"])
    team_members = get_team_members(path)
    print(team_members)
    team_members.to_csv('team_members.csv', index=False)
