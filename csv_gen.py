# DUO PROMPT: Iterate through Ruby files and read each line until the lines containing "author", "author_gitlab", "categories", "tags", and "guest" are found. Then print each of those lines.
import pandas as pd
import os

def read_ruby_files(path):
    # DUO PROMPT: Create a dataframe with the columns "author", "author_gitlab", "categories", "tag 1", "tag 2", "tag 3", "tag 4", and "guest"
    df = pd.DataFrame(columns=["date", "title", "author", "author_gitlab", "job_title", "reports_to", "categories", "guest", "url"])
    team_members = pd.read_csv("team_members.csv")

    # Iterate through all files in the given path
    for file in os.listdir(path):
        # Check if the file is a Ruby file
        if file.endswith(".erb"):
            # Read the file
            with open(os.path.join(path, file), "r") as f:
                lines = f.readlines()

            # Create a dictionary to store the values
            data = {"date": file[0:10], "url": "https://about.gitlab.com/blog/"+file[0:11].replace("-", "/") + file[11:].replace(".html.md.erb", "/")}

            # DUO PROMPT: Iterate through the lines and find the lines starting with "author", "author_gitlab", "categories", "tags", and "guest". Stop when a line is found containing "---" for the second time.
            start = False
            for line in lines:
                if line.startswith("author:"):
                    data["author"] = line.split(":")[1].split("#")[0].strip()
                elif line.startswith("author_gitlab:"):
                    data["author_gitlab"] = line.split(":")[1].split("#")[0].strip()
                    # DUO PROMPT: Search through the "gitlab" column of the team_members dataframe for the data["author_gitlab"] value. If found, set the "reports_to" value on that row of the dataframe as data["reports_to"] value.
                    try:
                        if "," not in data["author"]:
                            reports_to = team_members[team_members["gitlab"] == data["author_gitlab"]]["reports_to"].values
                            job_title = team_members[team_members["gitlab"] == data["author_gitlab"]]["job_title"].values
                        else:
                            temp_author = data["author"].split(",")[0].strip()
                            reports_to = team_members[team_members["name"] == temp_author]["reports_to"].values
                            job_title = team_members[team_members["name"] == temp_author]["job_title"].values
                        data["reports_to"] = reports_to[0]
                        data["job_title"] = job_title[0]
                    except:
                        print("Could not find reports_to and job_title for {}.".format(data["author"]))
                elif line.startswith("title:"):
                    data["title"] = line.split(":")[1].strip()
                elif line.startswith("categories:"):
                    data["categories"] = line.split(":")[1].strip()
                elif line.startswith("tags:"):
                    tags = line.split(":")[1].strip().split(",")
                    for i, tag in enumerate(tags):
                        data["tag {}".format(i + 1)] = tag.split(" # ")[0].strip()
                elif line.startswith("guest:"):
                    data["guest"] = line.split(":")[1].split("#")[0].strip()
                elif "---" in line:
                    if not start:
                        start = True
                    else:
                        break

            # Add the data to the dataframe
            if "ROLE" in os.environ:
                try:
                    if os.environ.get("ROLE") in data["job_title"]:
                        df = df.append(data, ignore_index=True)
                        print(data)
                except:
                    print("Could not find job_title for {}.".format(data["title"]))
            else:
                df = df.append(data, ignore_index=True)
                print(data)

    # Return the dataframe
    return df

if __name__ == "__main__":
    path = os.path.join(os.environ["FOLDER"], os.environ["REPO_DIR"])
    df = read_ruby_files(path)
    print(df)

    # Write the dataframe to a CSV file
    df.to_csv("blog_summary.csv", index=False)
